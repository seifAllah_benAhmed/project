/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/app/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      height: {
        hero: "calc(100vh - 64px)",
      },
      animation: {
        Loading: "loading 1s infinite",
      },
      keyframes: {
        loading: {
          "0%": {
            opacity: 0.1,
          },
          "30%": {
            opacity: 0.8,
          },
          "100%": {
            opacity: 0.1,
          },
        },
      },
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
