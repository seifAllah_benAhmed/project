import { NextAuthOptions } from "next-auth";
import GoogleProvider from "next-auth/providers/google";
import CredentialsProvider from "next-auth/providers/credentials";

function getGoogleCredentials() {
  const clientId = process.env.GOOGLE_CLIENT_ID;
  const clientSecret = process.env.GOOGLE_CLIENT_SECRET;

  if (!clientId || !clientId.length) {
    throw new Error("Missing GOOGLE_CLIENT_ID");
  }

  if (!clientSecret || !clientSecret.length) {
    throw new Error("Missing GOOGLE_CLIENT_SECRET");
  }

  return { clientId, clientSecret };
}

export const authOptions: NextAuthOptions = {
  session: {
    strategy: "jwt",
  },
  pages: {
    signIn: "/login",
  },
  providers: [
    GoogleProvider({
      clientId: getGoogleCredentials().clientId,
      clientSecret: getGoogleCredentials().clientSecret,
    }),
    CredentialsProvider({
      type: "credentials",
      credentials: {},
      authorize(credential, req) {
        const { email, password } = credential as {
          email: string;
          password: string;
        };
        if (email !== "seif@seif.com" && password !== "1234") {
          throw new Error("Invalid email or password");
        }

        return { id: "1", name: "seif", email: "seif@seif.com" };
      },
    }),
  ],
  callbacks: {
    redirect() {
      return "/";
    },
  },
};
