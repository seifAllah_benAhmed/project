"use client";
import { HydrateProps, Hydrate as HydrateQuerry } from "@tanstack/react-query";

export default async function Hydrate(props: HydrateProps) {
  return <HydrateQuerry {...props} />;
}
