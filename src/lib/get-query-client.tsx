import { QueryClient } from "@tanstack/react-query";
import { cache } from "react";

const getQueryClient = cache(() => new QueryClient());
export default getQueryClient;

// To Prefetch Data on the Server
//   const queryClient = getQueryClient();
//   await queryClient.prefetchQuery(["Key"],'Handler')
//   const dehydratedState = dehydrate(queryClient)
// then wrappe your Component
//   const queryClient = getQueryClient();
//   await queryClient.prefetchQuery(["Key"],'Handler')
//   const dehydratedState = dehydrate(queryClient)
//	 <Hydrate state={dehydratedState}>
// 		<Component />
// 	 </Hydrate>
// Component
//	 import { useQuery } from '@tanstack/react-query'

// export default function Posts() {
//   const { data } = useQuery({ queryKey: ['Key'], queryFn: Handler })
// ...
// }
