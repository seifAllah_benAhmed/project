import { getToken } from "next-auth/jwt";
import { withAuth } from "next-auth/middleware";
import { NextResponse } from "next/server";

export default withAuth(
  async function middleware(req) {
    const { pathname } = req.nextUrl;
    const isAuth = await getToken({ req });

    const isLoginPage = pathname.startsWith("/login");
    const isRegisterPage = pathname.startsWith("/register");
    const privateRoute = !isRegisterPage && !isLoginPage;
    if (isLoginPage || isRegisterPage) {
      return isAuth
        ? NextResponse.redirect(new URL("/", req.url))
        : NextResponse.next();
    }

    if (privateRoute && !isAuth) {
      return NextResponse.redirect(new URL("/login", req.url));
    }
  },
  {
    callbacks: {
      async authorized() {
        return true;
      },
    },
  }
);

export const config = {
  match: ["/((?!api|_next/static|_next/image|favicon.ico).*)"],
};
