import Button from "@components/ui/button";
import Input from "@components/ui/input";
import { Dialog, Transition } from "@headlessui/react";
import { PlusIcon } from "@heroicons/react/24/outline";
import { FC, Fragment, useState } from "react";

interface AddModalProps {}

const AddModal: FC<AddModalProps> = ({}) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }
  return (
    <>
      <Button variant="secondary" onClick={openModal}>
        <PlusIcon className="h-4 w-4 sm:mr-2.5" />
        <span className="hidden sm:block">Add post</span>
      </Button>
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-4xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="text-xl font-medium leading-6 text-gray-900"
                  >
                    Add new post
                  </Dialog.Title>
                  <form className="mt-4 space-y-4 md:space-y-6">
                    <div className="grid grid-cols-2 gap-4">
                      <Input type="date" name="date" id="date" label="Date" />
                      <Input type="time" name="time" id="time" label="Time" />
                    </div>
                    <div className="grid grid-cols-2 gap-4">
                      <Input
                        type="number"
                        name="number"
                        id="number"
                        label="Number of participant"
                        placeholder="50"
                      />
                      <Input
                        type="number"
                        name="price"
                        id="price"
                        label="Price per participant"
                        placeholder="2"
                      />
                    </div>
                    <Input
                      type="text"
                      name="location"
                      id="location"
                      label="Location"
                    />
                  </form>

                  <div className="mt-4">
                    <Button
                      type="button"
                      variant="secondary"
                      onClick={closeModal}
                    >
                      Submit
                    </Button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default AddModal;
