import Avatar from "@components/ui/avatar";
import Button from "@components/ui/button";
import {
  CalendarIcon,
  CreditCardIcon,
  MapPinIcon,
  UsersIcon,
} from "@heroicons/react/24/outline";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

interface CardProps {
  id: number;
}

const Card: FC<CardProps> = ({ id }) => {
  return (
    <div className="w-full border shadow rounded-md overflow-hidden hover:shadow-2xl hover:scale-125 hover:delay-300 duration-300 delay-0 transition-all">
      <Link href={`/${id}`} className="relative group">
        <div className="relative w-full h-40 overflow-hidden clip-polygon">
          <Image
            src="https://images.unsplash.com/photo-1522778119026-d647f0596c20?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            fill
            alt="stade"
            sizes="256w"
            className="group-hover:brightness-105"
          />
          <div
            aria-hidden="true"
            className="absolute inset-x-0 bottom-0 h-36 bg-gradient-to-tr from-emerald-800 opacity-50"
          />
        </div>
        <h2 className="text-2xl uppercase py-2 px-1 text-white text-red w-1/2 absolute bottom-0 right-0 leading-snug select-none">
          <span className="box-decoration-clone bg-gradient-to-tr p-1 from-emerald-500 to-emerald-600">
            Terrain De Football
          </span>
        </h2>
      </Link>
      <div className="grid grid-cols-2 gap-2.5 p-2.5">
        <p className="flex items-center justify-start">
          <MapPinIcon className="text-emerald-600 h-6 w-6 mr-2.5" />
          <span className="leading-none text-xs text-gray-500 font-medium">
            Les berges du lac
          </span>
        </p>
        <p className="flex items-center justify-start">
          <CalendarIcon className="text-emerald-600 h-6 w-6 mr-2.5" />
          <span className="leading-none text-xs text-gray-500 font-medium">
            Mai 02 at 16:00
          </span>
        </p>
        <p className="flex items-center justify-start">
          <UsersIcon className="text-emerald-600 h-6 w-6 mr-2.5" />
          <span className="leading-none text-xs text-gray-500 font-medium">
            2 Place left
          </span>
        </p>
        <p className="flex items-center justify-start">
          <CreditCardIcon className="text-emerald-600 h-6 w-6 mr-2.5" />
          <span className="leading-none text-xs text-gray-500 font-medium">
            10$ / Participant
          </span>
        </p>
      </div>
      <div className="p-2.5 bg-gray-100 grid grid-cols-2 gap-2.5 items-center justify-between">
        <Avatar />
        <Button className="justify-center"> Join now</Button>
      </div>
    </div>
  );
};

export default Card;
