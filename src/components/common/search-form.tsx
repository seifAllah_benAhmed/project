import Button from "@components/ui/button";
import SelectInput from "@components/ui/select-input";
import { FC } from "react";

interface SearchFormProps {}

const SearchForm: FC<SearchFormProps> = ({}) => {
  return (
    <>
      <h2 className="block text-2xl font-medium text-gray-700 m-6">Search:</h2>
      <div className="grid grid-cols-4 gap-5 px-5 pb-6 shadow-sm max-w-6xl m-auto w-full">
        <div className="col-span-2">
          <SelectInput />
        </div>
        <SelectInput />
        <SelectInput />
        {/* <div className="col-span-full">
          <Input type="text" />
        </div> */}

        <Button className="col-start-4 justify-center">Search</Button>
      </div>
    </>
  );
};

export default SearchForm;
