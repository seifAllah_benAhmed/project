"use client";
import Button from "@components/ui/button";
import Input from "@components/ui/input";
import { useMutation } from "@tanstack/react-query";
import { FC } from "react";
import { toast } from "react-hot-toast";
import { signIn } from "next-auth/react";

interface LoginProps {}

const Login: FC<LoginProps> = ({}) => {
  const { isLoading, mutate } = useMutation({
    mutationFn: () => signIn("google"),
    onError() {
      toast.error("Something went wrong with your login.");
    },
  });
  return (
    <>
      <form className="space-y-4 md:space-y-6">
        <Input
          id="email"
          type="email"
          name="email"
          label="Your email"
          placeholder="name@company.com"
        />

        <Input
          type="password"
          id="password"
          name="password"
          label="Password"
          placeholder="••••••••"
        />
        <label className="flex items-center h-5">
          <input
            type="checkbox"
            className="w-4 h-4 border text-emerald-600 border-gray-300 rounded bg-gray-50 shadow-sm focus:border-emerald-300 focus:ring focus:ring-offset-0 focus:ring-emerald-200 focus:ring-opacity-50"
          />
          <span className="ml-3 text-sm text-gray-500">Remember me</span>
        </label>
        <Button type="submit" className="w-full justify-center">
          Sign in
        </Button>
      </form>

      {/* Divider */}
      <div className="mt-6">
        <div className="relative">
          <div className="absolute inset-0 flex items-center">
            <div className="w-full border-t border-gray-300" />
          </div>
          <div className="relative flex justify-center text-sm">
            <span className="px-2 bg-white text-gray-500">
              Or continue with
            </span>
          </div>
        </div>

        {/* Social Auth */}
        <div className="mt-6 grid grid-cols-3 gap-3">
          <div>
            <Button variant="icon-square" className="w-full">
              <svg
                className="w-5 h-5"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
              >
                <path
                  fillRule="evenodd"
                  d="M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z"
                  clipRule="evenodd"
                />
              </svg>
            </Button>
          </div>

          <div>
            <Button variant="icon-square" className="w-full">
              <svg
                className="w-5 h-5"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 20 20"
              >
                <path d="M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0020 3.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.073 4.073 0 01.8 7.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 010 16.407a11.616 11.616 0 006.29 1.84" />
              </svg>
            </Button>
          </div>
          <div>
            <Button
              isLoading={isLoading}
              variant="icon-square"
              onClick={() => mutate()}
              className="w-full"
            >
              <svg
                className="w-5 h-5 scale-[calc(1000 / 20)]"
                aria-hidden="true"
                fill="currentColor"
                viewBox="0 0 48 48"
              >
                <path d="M43 24.4313C43 23.084 42.8767 21.7885 42.6475 20.5449H24.3877V27.8945H34.8219C34.3724 30.2695 33.0065 32.2818 30.9532 33.6291V38.3964H37.2189C40.885 35.0886 43 30.2177 43 24.4313Z" />
                <path d="M24.3872 43.001C29.6219 43.001 34.0107 41.2996 37.2184 38.3978L30.9527 33.6305C29.2165 34.7705 26.9958 35.4441 24.3872 35.4441C19.3375 35.4441 15.0633 32.1018 13.5388 27.6108H7.06152V32.5337C10.2517 38.7433 16.8082 43.001 24.3872 43.001Z" />
                <path d="M13.5395 27.6094C13.1516 26.4695 12.9313 25.2517 12.9313 23.9994C12.9313 22.7472 13.1516 21.5295 13.5395 20.3894V15.4668H7.06217C5.74911 18.0318 5 20.9336 5 23.9994C5 27.0654 5.74911 29.9673 7.06217 32.5323L13.5395 27.6094Z" />
                <path d="M24.3872 12.5568C27.2336 12.5568 29.7894 13.5155 31.7987 15.3982L37.3595 9.94866C34.0018 6.88281 29.6131 5 24.3872 5C16.8082 5 10.2517 9.25777 7.06152 15.4674L13.5388 20.39C15.0633 15.8991 19.3375 12.5568 24.3872 12.5568Z" />{" "}
              </svg>
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
