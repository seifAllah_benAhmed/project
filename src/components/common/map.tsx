"use client";
import { FC } from "react";
import { LatLngExpression } from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

interface MapProps {
  position: LatLngExpression;
}

const Map: FC<MapProps> = ({ position }) => {
  return (
    <MapContainer
      center={position}
      zoom={15}
      scrollWheelZoom={false}
      className="h-full"
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={position}>
        <Popup>
          Terrain De Football <br /> Les berges du lac.
        </Popup>
      </Marker>
    </MapContainer>
  );
};

export default Map;
