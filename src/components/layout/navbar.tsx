"use client";

import AddModal from "@components/common/add-modal";
import { Menu } from "@headlessui/react";
import { BellIcon } from "@heroicons/react/24/outline";
import { cn } from "@lib/utils";
import { useMutation } from "@tanstack/react-query";
import { SignOutParams, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import { toast } from "react-hot-toast";

const user = {
  name: "Tom Cook",
  email: "tom@example.com",
  imageUrl:
    "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
};

const userNavigation = [
  { name: "Your Profile", href: "#" },
  { name: "Settings", href: "#" },
  { name: "Sign out" },
];

export default function Navbar() {
  const [imageError, setImageError] = useState<boolean>(false);
  const { mutate } = useMutation({
    mutationFn: (options?: SignOutParams<true> | undefined) => signOut(options),
    onError() {
      toast.error("Something went wrong with your logout.");
    },
  });

  const session = useSession();
  return (
    <div className="min-h-full sticky top-0 z-50 bg-white">
      <div className="max-w-7xl mx-auto px-4 md:px-0 3xl:px-8">
        <div className="flex justify-between h-16">
          <div className="flex">
            <Link href="/" className="relative flex-shrink-0 flex items-center">
              <Image
                width={150}
                height={60}
                className="h-10 w-auto"
                src="/logo-desktop.png"
                alt="Workflow"
              />
            </Link>
          </div>
          <div className="sm:ml-6 flex items-center gap-2.5">
            <AddModal />
            <button
              type="button"
              className="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-emerald-500"
            >
              <BellIcon className="h-6 w-6" aria-hidden="true" />
            </button>
            {/* Profile dropdown */}
            <Menu as="div" className="ml-3 relative">
              <div>
                <Menu.Button className="max-w-xs h-8 w-8 relative bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-emerald-500">
                  <span className="sr-only">Open user menu</span>
                  {session.status === "loading" ? (
                    <div role="status" className="animate-pulse">
                      <div className="h-8 w-8  bg-gray-300 rounded-full dark:bg-gray-700" />
                    </div>
                  ) : !imageError ? (
                    <Image
                      height={250}
                      width={250}
                      className="rounded-full"
                      src={session.data?.user?.image || user.imageUrl}
                      alt="avatar"
                      onError={() => setImageError(true)}
                    />
                  ) : (
                    <svg
                      className="absolute w-8 h-8 text-gray-300"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"></path>
                    </svg>
                  )}
                </Menu.Button>
              </div>

              <Menu.Items className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                {userNavigation.map((item) => (
                  <Menu.Item key={item.name}>
                    {({ active }) =>
                      item.href ? (
                        <Link
                          href={item.href}
                          className={cn(
                            active ? "bg-gray-100" : "",
                            "block px-4 py-2 text-sm text-gray-700"
                          )}
                        >
                          {item.name}
                        </Link>
                      ) : (
                        <button
                          type="button"
                          onClick={() => mutate({})}
                          className={cn(
                            active ? "bg-gray-100" : "",
                            "block w-full px-4 py-2 text-sm text-gray-700 text-start"
                          )}
                        >
                          {item.name}
                        </button>
                      )
                    }
                  </Menu.Item>
                ))}
              </Menu.Items>
            </Menu>
          </div>
        </div>
      </div>
    </div>
  );
}
