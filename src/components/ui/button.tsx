import { cn } from "@lib/utils";
import { cva, VariantProps } from "class-variance-authority";
import { ButtonHTMLAttributes, FC } from "react";

const buttonVariant = cva(
  "inline-flex items-center border border-transparent border border-transparent font-medium shadow-sm focus:outline-none rounded-md active:scale-95 transition-color disabled:opacity-50 disabled:pointer-events-none",
  {
    variants: {
      variant: {
        primary: "text-white bg-emerald-600 hover:bg-emerald-700",
        secondary: "text-emerald-700 bg-emerald-100 hover:bg-emerald-200",
        icon: "bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-emerald-500 !hover:bg-none",
        "icon-square":
          "justify-center border border-gray-300  bg-white font-medium text-gray-500 hover:bg-emerald-200 hover:text-emerald-700",
      },
      size: {
        xs: "px-2.5 py-1.5  text-xs",
        sm: "px-3 py-2 text-sm leading-4",
        md: "px-4 py-2 text-sm",
        lg: "px-4 py-2 text-base",
      },
    },
    defaultVariants: {
      variant: "primary",
      size: "md",
    },
  }
);

interface ButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    VariantProps<typeof buttonVariant> {
  isLoading?: boolean;
}

const Button: FC<ButtonProps> = ({
  className,
  children,
  variant,
  isLoading,
  size,
  ...props
}) => {
  return (
    <button
      className={cn(buttonVariant({ variant, size, className }))}
      disabled={isLoading}
      {...props}
    >
      {isLoading ? <h1>Loading...</h1> : null}
      {children}
    </button>
  );
};

export default Button;
