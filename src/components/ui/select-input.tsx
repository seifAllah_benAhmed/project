"use client";
import { FC, Fragment, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { CheckIcon, ChevronUpDownIcon } from "@heroicons/react/20/solid";
import { cn } from "@lib/utils";
import { MapPinIcon } from "@heroicons/react/24/outline";

interface SelectInputProps {}
const terr = [
  { id: 1, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 2, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 3, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 4, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 5, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 6, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 7, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 8, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 9, name: "Terain Football Lac", adress: "Les berges du lac" },
  { id: 10, name: "Terain Football Lac", adress: "Les berges du lac" },
];

const SelectInput: FC<SelectInputProps> = ({}) => {
  const [selected, setSelected] = useState(terr[3]);
  return (
    <Listbox as="div" value={selected} onChange={setSelected}>
      {({ open }) => (
        <>
          <Listbox.Label className="block text-sm font-medium text-gray-700 m-2">
            By Location
          </Listbox.Label>
          <div className="mt-1 relative">
            <Listbox.Button className="bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-emerald-600 focus:border-emerald-600 sm:text-sm">
              <span className="block truncate">{selected.name}</span>
              <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <ChevronUpDownIcon
                  className="h-5 w-5 text-gray-400"
                  aria-hidden="true"
                />
              </span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                {terr.map((item) => (
                  <Listbox.Option
                    key={item.id}
                    className={({ active }) =>
                      cn(
                        active ? "text-white bg-emerald-600" : "text-gray-900",
                        "cursor-default select-none relative py-2 pl-3 pr-9"
                      )
                    }
                    value={item}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={cn(
                            selected ? "font-semibold" : "font-medium",
                            "block truncate"
                          )}
                        >
                          {item.name}
                        </span>
                        <span
                          className={cn(
                            selected ? "font-semibold" : "font-medium",
                            active
                              ? "text-white bg-emerald-600"
                              : "text-gray-400",
                            "block truncate "
                          )}
                        >
                          @{item.adress}
                        </span>

                        {selected ? (
                          <span
                            className={cn(
                              active ? "text-white" : "text-emerald-600",
                              "absolute inset-y-0 right-0 flex items-center pr-4"
                            )}
                          >
                            <CheckIcon className="h-5 w-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>
          </div>
        </>
      )}
    </Listbox>
  );
};

export default SelectInput;
