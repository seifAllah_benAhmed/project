import { cn } from "@lib/utils";
import { ClassValue } from "clsx";
import { FC, InputHTMLAttributes } from "react";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  labelClassName?: ClassValue;
}

const Input: FC<InputProps> = ({
  label,
  labelClassName,
  className,
  ...props
}) => {
  return (
    <div>
      {label ? (
        <label
          htmlFor={props.id}
          className={cn(
            "block mb-2 text-sm font-medium text-gray-900",
            labelClassName
          )}
        >
          {label}
        </label>
      ) : null}
      <input
        className={cn(
          "bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-emerald-600 focus:border-emerald-600 block w-full p-2.5",
          className
        )}
        {...props}
      />
    </div>
  );
};

export default Input;
