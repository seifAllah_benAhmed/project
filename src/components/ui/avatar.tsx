import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

interface AvatarProps {}

const Avatar: FC<AvatarProps> = ({}) => {
  return (
    <Link href="/" className="flex-shrink-0 group block">
      <div className="flex items-center">
        <div className="relative h-8 w-8">
          <Image
            fill
            className="rounded-full group-hover:brightness-105"
            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
            alt="avatar"
          />
        </div>
        <div className="ml-3 max-w-[66.666667%]">
          <p className="text-xs font-medium text-gray-500 group-hover:text-gray-700">
            Organized by
          </p>
          <p className="text-sm font-medium truncate text-gray-700 group-hover:text-gray-900">
            Med Amine
          </p>
        </div>
      </div>
    </Link>
  );
};

export default Avatar;
