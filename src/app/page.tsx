import Card from "@components/common/card";
import SearchForm from "@components/common/search-form";
import Navbar from "@components/layout/navbar";

export default function Home() {
  return (
    <>
      <Navbar />
      <main className="flex min-h-screen flex-col items-start justify-center w-full max-w-8xl mx-auto px-4 py-10">
        <div className="bg-slate-200 w-full max-w-6xl rounded-md mx-auto mb-10">
          <SearchForm />
        </div>
        <div className="grid sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-10 mx-auto w-96 max-w-full sm:w-fit">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((i) => (
            <Card key={i} id={i} />
          ))}
        </div>
      </main>
    </>
  );
}
