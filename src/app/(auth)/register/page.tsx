import Button from "@components/ui/button";
import Input from "@components/ui/input";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

interface PageProps {}

const Page: FC<PageProps> = ({}) => {
  return (
    <>
      <Link
        href="/"
        className="relative w-44 h-20 flex items-center mb-6 text-2xl font-semibold text-gray-900"
      >
        <Image fill src="/logo-desktop.png" alt="logo" priority />
      </Link>
      <div className="w-full bg-white rounded-lg shadow md:mt-0 sm:max-w-md xl:p-0">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
            Create an account
          </h1>
          <form className="space-y-4 md:space-y-6">
            <Input
              id="email"
              type="email"
              name="email"
              label="Your email"
              placeholder="name@company.com"
            />

            <Input
              id="password"
              type="password"
              name="password"
              label="Password"
              placeholder="••••••••"
            />
            <Input
              id="confirm-password"
              type="password"
              name="confirm-password"
              label="Confirm password"
              placeholder="••••••••"
            />
            <label className="flex items-center h-5">
              <input
                type="checkbox"
                className="w-4 h-4 border text-emerald-600 border-gray-300 rounded bg-gray-50 shadow-sm focus:border-emerald-300 focus:ring focus:ring-offset-0 focus:ring-emerald-200 focus:ring-opacity-50"
              />
              <span className="ml-3 text-sm text-gray-500 select-none">
                Remember me
              </span>
            </label>
            <Button type="submit" className="w-full justify-center">
              Create an account
            </Button>
            <p className="text-sm font-light text-gray-500 ">
              Already have an account?
              <Link
                href="/login"
                className="ml-2 font-medium text-emerald-600 hover:underline "
              >
                Login here
              </Link>
            </p>
          </form>
        </div>
      </div>
    </>
  );
};

export default Page;
