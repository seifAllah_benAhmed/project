import Login from "@components/common/forms/login";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

interface PageProps {}

const Page: FC<PageProps> = ({}) => {
  return (
    <>
      <Link
        href="/"
        className="relative w-44 h-20 flex items-center mb-6 text-2xl  flex-shrink-0 font-semibold text-gray-900 "
      >
        <Image fill src="/logo-desktop.png" alt="logo" priority />
      </Link>
      <div className="w-full bg-white rounded-lg shadow md:mt-0 sm:max-w-md xl:p-0">
        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
          <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
            Sign in to your account
          </h1>

          <Login />

          <p className="text-sm text-gray-500 ">
            Don’t have an account yet?
            <Link
              href="/register"
              className="ml-2 font-medium text-emerald-600 hover:underline "
            >
              Sign up
            </Link>
          </p>
        </div>
      </div>
    </>
  );
};

export default Page;
