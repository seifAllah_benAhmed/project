import Navbar from "@components/layout/navbar";
import Avatar from "@components/ui/avatar";
import {
  UserIcon,
  StarIcon,
  ClockIcon as Clock,
  MapPinIcon,
} from "@heroicons/react/24/outline";
import dynamic from "next/dynamic";
import Image from "next/image";
import { FC } from "react";

const Map = dynamic(() => import("@components/common/map"), { ssr: false });

interface PageProps {}
// export const dynamic = "force-dynamic";

const Page: FC<PageProps> = ({}) => {
  return (
    <main className="bg-gray-100">
      <Navbar />
      <section className="relative h-hero">
        <div className="relative w-full h-full overflow-hidden clip-polygon">
          <Image
            src="https://images.unsplash.com/photo-1522778119026-d647f0596c20?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            fill
            alt="stade"
            sizes="256w"
            className="group-hover:brightness-105"
          />
          <div
            aria-hidden="true"
            className="absolute inset-x-0 bottom-0 h-full bg-gradient-to-tr from-black opacity-60"
          />
        </div>
        <div className="absolute bottom-1/2 right-1/2 translate-x-1/2 translate-y-1/2 w-full text-center">
          <h2 className="text-4xl uppercase py-2 px-1 text-white leading-snug select-none">
            <span className="box-decoration-clone bg-gradient-to-tr p-2 from-emerald-500 to-emerald-600">
              Terrain De Football
            </span>
          </h2>
        </div>
      </section>
      <section className="clip-polygon container p-24 mb-20 grid grid-cols-2 gap-8">
        <div className="">
          <h2 className="font-medium text-transparent uppercase text-3xl bg-clip-text bg-gradient-to-r from-[#7ed56f] to-[#28b485] mb-5">
            quick facts
          </h2>
          <ul className="grid gap-2.5 mb-5">
            <li className="flex items-center justify-start">
              <Clock className="text-emerald-600 w-6 h-6 mr-2.5" />
              <span className="leading-none text-base text-gray-500 font-medium">
                Mai 02 at 16:00
              </span>
            </li>
            <li className="flex items-center justify-start">
              <MapPinIcon className="text-emerald-600 w-6 h-6 mr-2.5" />
              <span className="leading-none text-base text-gray-500 font-medium">
                Ben arous, Megrine
              </span>
            </li>
            <li className="flex items-center justify-start">
              <UserIcon className="text-emerald-600 w-6 h-6 mr-2.5" />
              <span className="leading-none text-base text-gray-500 font-medium">
                2 Place left
              </span>
            </li>
            <li className="flex items-center justify-start">
              <StarIcon className="text-emerald-600 w-6 h-6 mr-2.5" />
              <span className="leading-none text-base text-gray-500 font-medium">
                5 / 5
              </span>
            </li>
          </ul>
          <h2 className="font-medium text-transparent uppercase text-3xl bg-clip-text bg-gradient-to-r from-[#7ed56f] to-[#28b485] mb-5">
            Organized by
          </h2>
          <ul>
            <li>
              <Avatar />
            </li>
          </ul>
        </div>
        <div>
          <h2 className="font-medium text-transparent uppercase text-3xl bg-clip-text bg-gradient-to-r from-[#7ed56f] to-[#28b485] mb-5">
            About Terrain De Football Event
          </h2>
          <p className="text-gray-500 leading-relaxed text-justify">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fuga
            beatae aliquam facilis optio consequuntur minus in, quisquam illo
            alias iure corporis, ratione quos! Harum quod nemo, dignissimos sit
            nisi expedita! Lorem ipsum dolor sit amet consectetur, adipisicing
            elit. Fuga beatae aliquam facilis optio consequuntur minus in,
            quisquam illo alias iure corporis, ratione quos! Harum quod nemo,
            dignissimos sit nisi expedita! Lorem ipsum dolor sit amet
            consectetur, adipisicing elit. Fuga beatae aliquam facilis optio
            consequuntur minus in, quisquam illo alias iure corporis, ratione
            quos! Harum quod nemo, dignissimos sit nisi expedita!
          </p>
        </div>
      </section>
      <section className="h-hero w-full -mt-10 clip-polygon-reverse">
        <Map position={[51.505, -0.09]} />
      </section>
    </main>
  );
};

export default Page;
